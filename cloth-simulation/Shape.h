#pragma once
#include "Utility.h"
#include "Particle.h"

class Shape
{
public:
	Shape();
	~Shape();

	void initialize(const char* texture, const char* vertexShader, const char* fragmentShader, float startX, float startY, float startZ, float xScaleValue, float yScaleValue);
	GLuint getProgram();
	void render(glm::mat4);

	void addMovementX(float);
	void addMovementY(float);

	float getX();
	float getY();
	void setX(float);
	void setY(float);
	void updateTime(GLfloat);

private:
	GLuint VAO;
	GLuint VBO;
	GLuint EBO;
	GLuint Program;
	GLuint baseTexture;
	GLfloat currentTime;

	GLfloat QuadVertices[32];
	GLuint QuadIndices[6];

	glm::vec3 position;
	glm::vec3 scale;

	std::vector<Particle> points;

};

