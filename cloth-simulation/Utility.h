#pragma once

//Graphics
#include <glew.h>
#include <freeglut.h>
#include <SOIL.h>

//Sound
#include <fmod.hpp>

//Camera
#include "glm.hpp"
#include "gtc/matrix_transform.hpp"
#include "gtc/type_ptr.hpp"
#include "glm/gtx/rotate_vector.hpp"

//Text
#include <ft2build.h>
#include FT_FREETYPE_H

//Misc
#include <iostream>
#include <vector>
#include <fstream>
#include <map>
#include <string>
#include <strstream>
#include <stdlib.h>
#include <time.h> 

//Class
#include "ShaderLoader.h"

#define _USE_MATH_DEFINES
#include <cmath>
#include <math.h>