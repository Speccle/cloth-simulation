#include "Camera.h"

Camera::Camera()
{
}


Camera::~Camera()
{
}

/**************************************
* Name of the Function: initialize
* @Author: Chloe Johnston
* @Parameter: n/a
* @Return: n/a
* @Discription: create view and projection matrix and combine
***************************************/
void Camera::initialize() {

	camPos = glm::vec3(0.0f, 0.0f, 15.0f);
	camLookDir = glm::vec3(0.0f, 0.0f, -1.0f);
	camUpDir = glm::vec3(0.0f, 1.0f, 0.0f);

	//create view matrix
	view = glm::lookAt(camPos, camPos + camLookDir, camUpDir);

	//create projection matrix
	proj = glm::perspective(45.0f, (float)SCR_WIDTH / SCR_HEIGHT, 0.1f, 10000.0f);

	//combine matrix
	VP = proj * view;
}

/**************************************
* Name of the Function: getVPMatrix
* @Author: Chloe Johnston
* @Parameter: n/a
* @Return: mat4
* @Discription: returns the combined view and prijection matrix
***************************************/
glm::mat4 Camera::getVPMatrix() {

	return VP;
}


void Camera::Update(float deltaTime, glm::vec3 position) {

	timeElapsed += deltaTime;
	GLfloat radius = 100.0f;
	camPos.x = position.x; //sin(timeElapsed) * radius;
	camPos.y = position.y + 20; // 50.0f;
	camPos.z = position.z + 50;  //100; //cos(timeElapsed) * radius;

	camLookDir.x = position.x;
	camLookDir.y = position.y;
	camLookDir.z = position.z;

	view = glm::lookAt(camPos, camLookDir, camUpDir);

	VP = proj * view;
}


glm::vec3 Camera::getCamPos() {

	return camPos;
}

glm::vec3 Camera::getUpDir() {

	return camUpDir;
}