#include "Shape.h"

Shape::Shape()
{
}

Shape::~Shape()
{
}

/**************************************
* Name of the Function: initialize
* @Author: Chloe Johnston
* @Parameter: char*, float, float, float, float, float
* @Return: n/a
* @Discription: initialize variables
***************************************/
void Shape::initialize(const char* texture, const char* vertexShader, const char* fragmentShader, float startX, float startY, float startZ, float xScaleValue, float yScaleValue)
{
	//create program
	Program = ShaderLoader::CreateProgram("Resources/Shaders/VertexShader.txt",
		"Resources/Shaders/QuadFragShader.txt");

	//assign variables
	position.x = startX;
	position.y = startY;
	position.z = startZ;
	scale.x = xScaleValue;
	scale.y = yScaleValue;

	// Bind Quad VAO
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	// Bind Quad EBO
	glGenBuffers(1, &EBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(QuadIndices), QuadIndices, GL_STATIC_DRAW);
	// Bind Quad VBO
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(QuadVertices), QuadVertices, GL_STATIC_DRAW);
	// Create Quad Posiion Pointer
	glVertexAttribPointer(
		0,
		3,
		GL_FLOAT,
		GL_FALSE,
		8 * sizeof(GLfloat),
		(GLvoid*)0
	);
	glEnableVertexAttribArray(0);
	// Create Quad Colour Pointer
	glVertexAttribPointer(
		1,
		3,
		GL_FLOAT,
		GL_FALSE,
		8 * sizeof(GLfloat),
		(GLvoid*)(3 * sizeof(GLfloat))
	);
	glEnableVertexAttribArray(1);
	// Create Quad Texture Pointer
	glVertexAttribPointer(
		2,
		2,
		GL_FLOAT,
		GL_FALSE,
		8 * sizeof(GLfloat),
		(GLvoid*)(6 * sizeof(GLfloat))
	);
	glEnableVertexAttribArray(2);

	// Bind First Texture
	glGenTextures(1, &baseTexture);
	glBindTexture(GL_TEXTURE_2D, baseTexture);

	//Load and Create First Texture
	int width, height;
	unsigned char* image = SOIL_load_image(texture, &width, &height, 0, SOIL_LOAD_RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glGenerateMipmap(GL_TEXTURE_2D);
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);

	//create and calculate model matrix
	glm::vec3 objPosition = glm::vec3(0.0f + startX, 0.0f + startY, 0.0f + startZ);
	glm::mat4 translationMatrix = glm::translate(glm::mat4(), objPosition);
	glm::vec3 rotationAxisZ = glm::vec3(0.0f, 0.0f, 1.0f);
	float rotationAngle = 0;
	glm::mat4 rotationZ = glm::rotate(glm::mat4(), glm::radians(rotationAngle), rotationAxisZ);
	glm::vec3 objScale = glm::vec3(scale.x, scale.y, 0.0f);
	glm::mat4 scaleMatrix = glm::scale(glm::mat4(), objScale);

	glm::mat4 model = translationMatrix * rotationZ * scaleMatrix;

}

/**************************************
* Name of the Function: Render
* @Author: Chloe Johnston
* @Parameter: mat4
* @Return: n/a
* @Discription: bind texture, calcualte matrix and draw elements
***************************************/
void Shape::render(glm::mat4 passedIn)
{
	glUseProgram(Program);

	//alpha blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//bind texture
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, baseTexture);
	glUniform1i(glGetUniformLocation(Program, "tex"), 0);

	//combine model matrix with passed in camera matric
	glm::mat4 combined = passedIn;

	//send combined matrix to shaders
	GLuint combinedLoc = glGetUniformLocation(Program, "combined");
	glUniformMatrix4fv(combinedLoc, 1, GL_FALSE, glm::value_ptr(combined));

	//draw elements
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);

	//disable alpha blend
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

}

/**************************************
* Name of the Function: getProgram
* @Author: Chloe Johnston
* @Parameter: n/a
* @Return: GLuint
* @Discription: return each shapes program
***************************************/
GLuint Shape::getProgram()
{
	return Program;
}

/**************************************
* Name of the Function: addMovementX
* @Author: Chloe Johnston
* @Parameter: float
* @Return: void
* @Discription: changes the x position of the shape by a passed in variable
***************************************/
void Shape::addMovementX(float add)
{
	position.x = add;
}

/**************************************
* Name of the Function: addMovementY
* @Author: Chloe Johnston
* @Parameter: float
* @Return: void
* @Discription: changes the y position of the shape by a passed in variable
***************************************/
void Shape::addMovementY(float add)
{
	position.y = add;
}

/**************************************
* Name of the Function: getX
* @Author: Chloe Johnston
* @Parameter: n/a
* @Return: float
* @Discription: returned the x position
***************************************/
float Shape::getX() {
	return position.x;
}

/**************************************
* Name of the Function: getY
* @Author: Chloe Johnston
* @Parameter: n/a
* @Return: float
* @Discription: returned the y position
***************************************/
float Shape::getY() {
	return position.y;
}

/**************************************
* Name of the Function: setX
* @Author: Chloe Johnston
* @Parameter: float
* @Return: void
* @Discription: changes the x position to new position
***************************************/
void Shape::setX(float newX) {
	position.x = newX;
}

/**************************************
* Name of the Function: setY
* @Author: Chloe Johnston
* @Parameter: float
* @Return: void
* @Discription: changes the y position to new position
***************************************/
void Shape::setY(float newY) {
	position.y = newY;
}

/**************************************
* Name of the Function: updateTime
* @Author: Chloe Johnston
* @Parameter: GLfloat
* @Return: void
* @Discription: changes currentTime value to passed in variable value
***************************************/
void Shape::updateTime(GLfloat add)
{
	currentTime = add;
}