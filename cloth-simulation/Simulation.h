#include "ShaderLoader.h"
#include "Shape.h"

#pragma once

class Simulation
{
public:
	Simulation();
	~Simulation();

	void Render();
	void Update();
	void Initialize();

	void ApplyForce(float force);
	void VerletIntegration();
	void SatisfyConstraints();

private: 

	std::vector<Particle> points;
	Camera

};

