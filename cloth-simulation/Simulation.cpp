#include "Simulation.h"



Simulation::Simulation()
{
}


Simulation::~Simulation()
{
}

void Simulation::Initialize()
{
	for (int i = 0; i < 4; i++) {
		Particle point = Particle();
		point.SetPosition(glm::vec3(10 * i, 1, -10));
		points.push_back(point);
	}
}

void Simulation::Update()
{

	for (int i = 0; i < points.size(); i++) {
		points.at(i).Update();
	}

}

void Simulation::Render()
{
	for (int i = 0; i < points.size(); i++) {
		points.at(i).Render();
	}
}
