#include "Utility.h"
#include "Simulation.h"


Simulation simulation;

void Render();
void Update();
void Initialize();
void KeyboardDown(unsigned char, int, int);
void KeyboardUp(unsigned char, int, int);
void MouseMove(int _x, int _y);
void ProcessInput();
bool AudioInit();

int main(int argc, char **argv) {

	//Setup and create at glut controlled window

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA | GLUT_MULTISAMPLE | GLUT_STENCIL);
	glutInitWindowPosition(400, 5);
	glutInitWindowSize(1000, 1000);
	glutCreateWindow("OpenGL Window");

	// Sets up all gl function callbacks based on pc hardware
	if (glewInit() != GLEW_OK)
	{
		// if glew setup failed then aplication will not run graphics correctly
		std::cout << "Glew Initialization Failed. Aborting Application" << std::endl;
		system("pAUse");
	}
#if _DEBUG
	// Sets the clear color when calling glClear()
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f); // White
#else
	// Sets the clear color when calling glClear()
	glClearColor(0.5f, .5f, 0.5f, 1.0f); // Black

#endif // DEBUG

	//Initialize everything
	Initialize();

	// Register callbacks
	glutKeyboardFunc(KeyboardDown);
	glutKeyboardUpFunc(KeyboardUp);
	glutPassiveMotionFunc(MouseMove);
	glutIdleFunc(Update);
	glutDisplayFunc(Render);

	glutMainLoop(); // Ensure this is the last glut line called
	return 0;
}

void Initialize() {

	simulation.Initialize();
}

void Render()
{
	simulation.Render();
}

void Update() {

	simulation.Update();
}

void MouseMove(int _x, int _y) {

}

void KeyboardDown(unsigned char key, int x, int y) {

}

void KeyboardUp(unsigned char key, int x, int y) {

}

void ProcessInput() {
	
}

bool AudioInit() {

	/*FMOD_RESULT result;
	result = FMOD::System_Create(&audioSystem);
	if (result != FMOD_OK) { return false; }

	result = audioSystem->init(100, FMOD_INIT_NORMAL || FMOD_INIT_3D_RIGHTHANDED, 0);
	if (result != FMOD_OK) { return false; }
	*/
	return true;
}