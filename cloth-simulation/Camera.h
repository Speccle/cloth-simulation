#pragma once
#include "Utility.h"

class Camera
{
public:
	Camera();
	~Camera();

	void initialize();
	glm::mat4 getVPMatrix();
	void Update(float, glm::vec3);

	glm::vec3 getCamPos();
	glm::vec3 getUpDir();

	glm::vec3 camLookDir;
	glm::vec3 position = { 15.0f, 0.0f, 50.0f };

private:

	glm::vec3 camPos;
	glm::vec3 camUpDir;

	glm::mat4 view;
	glm::mat4 proj;
	glm::mat4 VP;

	float timeElapsed = 0.0f;
};
