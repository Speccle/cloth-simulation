#include "Utility.h"
#include "Sphere.h"

#pragma once
class Particle
{
public:
	Particle();
	~Particle();

	void Update();
	void Render();

	void SetPosition(glm::vec3);
	glm::vec3 GetPosition();

private:

	glm::vec3 CPosition;
	glm::vec3 PPosition;
	glm::vec3 Velocity;
	Sphere ball;

};

