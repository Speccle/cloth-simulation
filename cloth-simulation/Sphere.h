#pragma once

#include "Utility.h"

class Sphere
{
public:
	Sphere();
	~Sphere();

	void Render();

private:
	GLuint VAO = 0;
	GLuint IndiceCount;
	int DrawType;
};

